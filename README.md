[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-backend/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-backend/-/commits/staging) [![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-backend/badges/staging/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-backend/-/commits/staging)

## Dietela Mobile Application (Backend)

This repository contains codebase for Dietela backend system

### About Dietela

Dietela is an Android-based application that provides personalised diet and online nutritionist consultation services.

### How to Run

1. Clone dietela-backend repository from gitlab

```
git clone https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-backend.git
```

2. cd into the project directory

```
cd dietela-backend
``` 

3. Install virtualenv if it doesn’t exist yet

```
pip install virtualenv
```

4. Prepare and activate a Python virtual environment

Windows
```
virtualenv venv
venv\Scripts\activate
```

Linux
```
virtualenv env
source env/bin/activate
```

5. Install required packages

```
pip install -r requirements.txt
```

6. Create a postgres database called dietela-backend for the main local database

```
# Login to your postgres account
CREATE DATABASE dietela_backend;
```

7. Create a postgres database called test-dietela-backend for the testing local database

```
# Login to your postgres account
CREATE DATABASE test_dietela_backend;
```

8. Create a `.env` file inside the root project, copy the variables below and configure it according to your postgres username and password:

```
DATABASE_URL=postgres://<username>:<password>@127.0.0.1:5432/dietela_backend
TEST_DATABASE=test-dietela_backend
```

9. Do database migrations

```
python manage.py migrate
```

10. Run

```
python manage.py runserver
```

### Deployment

1. Create a Heroku application through heroku.com

2. Setup postgres database in Heroku

    Create 2 postgres database add-ons through the heroku app’s resources page.
    
    One will be used for production and the other for testing.

3. Set CI/CD script by creating .gitlab-ci.yml file

    Create stages needed

    For this case, the stages are `test` for testing, `sonar-scanner` for sonar scanner, and `deploy` for deployment

    View [this file](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/pt-gizi-sehat-dietela/dietela-backend/-/blob/staging/.gitlab-ci.yml) for reference

4. Set variables needed in .gitlab-ci.yml file at Settings > CI/CD > Variables
 
    HEROKU_APIKEY = `your api key from your account settings`

    HEROKU_APP_HOST = `the heroku app host or url`

    HEROKU_APPNAME = `the heroku app name`
    
    SONARQUBE_PROJECT_KEY = `the project key of your sonarqube`
    
    SONARQUBE_TOKEN = `the token of your sonarqube`
    
    DATABASE_URL = `your database url from your heroku production database credential`
    
    TEST_DATABASE_URL = `your database url from your heroku testing database credential`
    
    TEST_DATABASE = `the database name from your heroku testing database credential`

5. Configure `settings.py`:

```
import dj_database_url
 
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'TEST': {
            'NAME': os.environ.get('TEST_DATABASE'),
        },
    }
}
 
# Heroku: Update database configuration from $DATABASE_URL.
db_from_env = dj_database_url.config(conn_max_age=600)
DATABASES['default'].update(db_from_env)
```

6. Create `deployment.sh` file

```
#!/bin/bash
python manage.py makemigrations
python manage.py migrate
```

7. Create `Procfile` file

```
migrate: bash deployment.sh
web: gunicorn dietela_backend.wsgi --log-file -
```

8. Commit and push to your repository


### Current Features

- PBI 1: Dietela Quiz & Profile Dietku (Client)

    Clients are required to fill out a quiz related to the design needs of the diet program that will be made by Dietela team

- PBI 2: Dietela Quiz & Profile Dietku (Dietela team)

    Dietela team can store client data to provide quiz results containing programs and nutritionists recommendation to clients

- PBI 3: Choose Plan

    Clients can continue by choosing recommended diet program that is suitable for their needs

- PBI 4: Registration

    Clients can register an account to access Dietela's authenticated-only features

- PBI 5: Login

    Clients can do authentication using a registered account to access Dietela's authenticated-only features

- PBI 6: Logout

    Clients can sign out from the Dietela app

- PBI 7: Checkout

    Clients can choose a diet program and a nutritionist then proceed to do the payment

- PBI 8: Payment Integration

    Clients can perform payment using a third party payment gateway (Midtrans)


### Developers


Team **we fall in love with PPL we cant have**

- Doan Andreas Nathanael - 1806205123
- Glenda Emanuella Sutanto - 1806133774
- Kefas Satrio Bangkit Solideantyo - 1806204972
- Muzaki Azami Khairevy - 1806205470
- Wulan Mantiri - 1806205666

## Acknowledgements
* CS UI - Software Engineering Project 2021
