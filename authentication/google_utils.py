import json
import requests
from django.http import JsonResponse
from rest_framework import status


def validate_google_token(access_token):
    payload = {'access_token': access_token}
    response = requests.get('https://www.googleapis.com/oauth2/v2/userinfo', params=payload)
    data = json.loads(response.text)

    if 'error' in data:
        content = {'message': 'wrong google token / this google token is already expired.'}
        return False, JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)

    return True, data
