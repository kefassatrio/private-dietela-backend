# Generated by Django 3.1 on 2021-04-24 06:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='name',
            field=models.CharField(default='tes', max_length=100),
            preserve_default=False,
        ),
    ]
