from django.contrib.auth.models import Group
from rest_framework import serializers, exceptions
from dj_rest_auth.serializers import LoginSerializer
from dj_rest_auth.registration.serializers import RegisterSerializer
from .models import CustomUser


class CustomUserDetailsSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        result = super().to_representation(instance)
        if instance.groups.exists():
            result['role'] = instance.groups.first().name
        if instance.is_staff:
            result['role'] = 'admin'
        return result

    class Meta:
        model = CustomUser
        fields = ('id', 'name', 'email',)
        read_only_fields = ('id', 'email',)


class CustomLoginSerializer(LoginSerializer):

    def validate_auth_user_status(self, user):
        request = self.context.get('request')
        if not request.data.get('role'):
            msg = 'Role is missing from the payload.'
            raise exceptions.ValidationError(msg)
        if not user.groups.filter(name=request.data.get('role')).exists():
            msg = 'Invalid role for the user.'
            raise exceptions.ValidationError(msg)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class CustomRegisterSerializer(RegisterSerializer):
    name = serializers.CharField()

    def get_cleaned_data(self):
        super().get_cleaned_data()
        return {
            'email': self.validated_data.get('email', ''),
            'password1': self.validated_data.get('password1', ''),
            'name': self.validated_data.get('name', '')
        }

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    def save(self, request):
        user = super().save(request)
        user.name = self.get_cleaned_data().get('name')
        user.save()
        client_role, _created = Group.objects.get_or_create(name='client')
        user.groups.add(client_role)
        return user
