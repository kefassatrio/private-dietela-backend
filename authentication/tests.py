import json
from unittest.mock import patch
from rest_framework import status
from rest_framework.test import APITestCase
from django.test import TestCase
from django.http import JsonResponse
from dietela_quiz.models import DietProfile
from payment.models import Cart
from dietela_program.models import DietelaProgram
from nutritionists.models import Nutritionist
from .models import CustomUser
from .serializers import (
    CustomLoginSerializer, CustomRegisterSerializer,CustomUserDetailsSerializer
)
from .google_utils import validate_google_token

class UserModelTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.diet_profile_1 = DietProfile.objects.create(
            name="test",
            email="test@test.com",
            age=21,
            weight=70,
            height=173,
            gender=1,
            special_condition=2,
            body_activity=2,
            vegetables_in_one_day=2,
            fruits_in_one_day=2,
            fried_food_in_one_day=2,
            sweet_snacks_in_one_day=2,
            sweet_drinks_in_one_day=2,
            packaged_food_in_one_day=2,
            large_meal_in_one_day=2,
            snacks_in_one_day=2,
            breakfast_type=2,
            current_condition=2,
            problem_to_solve=2,
            health_problem=[1])

        cls.diet_profile_2 = DietProfile.objects.create(
            name="test",
            email="email2@gmail.com",
            age=21,
            weight=70,
            height=173,
            gender=1,
            special_condition=2,
            body_activity=2,
            vegetables_in_one_day=2,
            fruits_in_one_day=2,
            fried_food_in_one_day=2,
            sweet_snacks_in_one_day=2,
            sweet_drinks_in_one_day=2,
            packaged_food_in_one_day=2,
            large_meal_in_one_day=2,
            snacks_in_one_day=2,
            breakfast_type=2,
            current_condition=2,
            problem_to_solve=2,
            health_problem=[1])

        cls.custom_user_1 = CustomUser.objects.create_user(name='tes', email='email@email.com', password='abc')

        cls.custom_user_2 = CustomUser.objects.create_user(name='tes', email='email2@gmail.com', password='abc')

        cls.inactive_user = CustomUser.objects.create_user(name='inactive', email='inactive@gmail.com', password='abc')
        cls.inactive_user.is_active = False
        cls.inactive_user.save()

        cls.num_of_diet_profile = DietProfile.objects.count()
        cls.num_of_custom_user = CustomUser.objects.count()

        cls.dietela_program = DietelaProgram.objects.create(
            unique_code="PRG1",
            name="Program 1",
            price=350000.00,
        )

        cls.nutritionist = Nutritionist.objects.create(
            id=1,
            full_name_and_degree="Test, S.Gz",
            registration_certificate_no="1234567890",
            university="Universitas Indonesia",
            mastered_nutritional_problems="Manajemen berat badan, hipertensi",
            handled_age_group="12-17 tahun (Remaja)",
            another_practice_place="RSCM",
            languages="Bahasa Indonesia, Bahasa Inggris",
        )

        cls.cart = Cart.objects.create(program=cls.dietela_program, nutritionist=cls.nutritionist)

    def test_create_user(self):
        user = CustomUser.objects.create_user(name='tes', email='email2@email.com',
            password='tes', diet_profile=self.diet_profile_1,)
        self.assertEqual(str(user), 'email2@email.com')
        self.assertEqual(user.email, 'email2@email.com')
        self.assertTrue(user.groups.filter(name='client').exists())
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(user.username)
        except AttributeError:
            pass
        with self.assertRaises(TypeError):
            CustomUser.objects.create_user()
        with self.assertRaises(TypeError):
            CustomUser.objects.create_user(email='')
        with self.assertRaises(ValueError):
            CustomUser.objects.create_user(email='', password="tes")

    def test_create_superuser(self):
        admin_user = CustomUser.objects.create_superuser('super@user.com', 'foo')
        self.assertEqual(admin_user.email, 'super@user.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(admin_user.username)
        except AttributeError:
            pass
        with self.assertRaises(ValueError):
            CustomUser.objects.create_superuser(
                email='super@user.com', password='foo', is_superuser=False)
        with self.assertRaises(ValueError):
            CustomUser.objects.create_superuser(
                email='super@user.com', password='foo', is_staff=False)

    def test_user_serializer(self):
        user_data = CustomUserDetailsSerializer(self.custom_user_1).data
        self.assertIn('email', user_data)
        self.assertIn('name', user_data)
        self.assertEqual('client', user_data.get('role'))
        self.assertIn('id', user_data)

        user_no_role = CustomUser()
        user_no_role.name = 'no role'
        user_no_role.email = 'norole@norole.com'
        user_no_role.password = 'norole2404'
        user_no_role.save()
        user_no_role_data = CustomUserDetailsSerializer(user_no_role).data
        self.assertTrue('role' not in user_no_role_data)

        admin_user = CustomUser.objects.create_superuser('super2@user.com', 'foo')
        admin_user_data = CustomUserDetailsSerializer(admin_user).data
        self.assertEqual('admin', admin_user_data.get('role'))

    def test_post_registration_user_succeed(self):
        data = {
            'name': 'tes',
            'email': 'abc123@gmail.com',
            'password1': '2828abab',
            'password2': '2828abab',
        }

        response = self.client.post('/auth/registration/', data, format='json')
        json_response = json.loads(response.content)
        self.assertEqual(json_response.get('user').get('email'), 'abc123@gmail.com')
        self.assertEqual(json_response.get('user').get('role'), 'client')
        self.assertEqual(json_response.get('user').get('name'), 'tes')
        self.assertEqual(CustomUser.objects.count(), self.num_of_custom_user + 1)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_registration_user_failed_because_invalid_email(self):
        data = {
            'email': 'abc123gmail.com',
            'password1': '2828abab',
            'password2': '2828abab',
        }

        response = self.client.post('/auth/registration/', data, format='json')
        self.assertEqual(CustomUser.objects.count(), self.num_of_custom_user)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_registration_user_failed_because_different_password(self):
        data = {
            'name': 'tes',
            'email': 'abc123@gmail.com',
            'password1': '2828abaab',
            'password2': '2828abab',
        }

        response = self.client.post('/auth/registration/', data, format='json')
        self.assertEqual(CustomUser.objects.count(), self.num_of_custom_user)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_registration_user_failed_because_email_already_registered(self):
        data = {
            'name': 'tes',
            'email': self.custom_user_1.email,
            'password1': '2828abaab',
            'password2': '2828abaab',
        }

        response = self.client.post('/auth/registration/', data, format='json')
        self.assertEqual(CustomUser.objects.count(), self.num_of_custom_user)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_registration_serializer_to_do_nothing(self):
        serializer = CustomRegisterSerializer()
        self.assertEqual(serializer.create({}), None)
    
    def test_update_registration_serializer_to_do_nothing(self):
        serializer = CustomRegisterSerializer()
        self.assertEqual(serializer.update(None, {}), None)

    def test_link_user_and_diet_profile_and_cart_succeed(self):
        data = {
            'email': self.custom_user_2.email,
            'diet_profile_id': self.diet_profile_2.id,
            'cart_id': self.cart.id,
        }

        response = self.client.post('/auth/link-data/', data, format='json')
        self.assertEqual(json.loads(response.content).get('user').get('email'), \
            self.custom_user_2.email)
        self.assertEqual(json.loads(response.content).get('diet_profile').get('id'), \
            self.diet_profile_2.id)
        self.assertEqual(json.loads(response.content).get('cart').get('id'), self.cart.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_link_user_and_diet_profile_failed_because_email_doesnt_exist(self):
        data = {
            'email': 'emaill@gmail.com',
            'diet_profile_id': self.diet_profile_2.id,
            'cart_id': self.cart.id,
        }

        response = self.client.post('/auth/link-data/', data, format='json')
        self.assertEqual(json.loads(response.content).get('message'), 'User is not found.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_link_user_and_diet_profile_failed_because_diet_profile_doesnt_exist(self):
        data = {
            'email': self.custom_user_2.email,
            'diet_profile_id': -1,
            'cart_id': self.cart.id,
        }

        response = self.client.post('/auth/link-data/', data, format='json')
        self.assertEqual(json.loads(response.content).get('message'), 'Diet profile is not found.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_link_user_and_diet_profile_failed_because_cart_doesnt_exist(self):
        data = {
            'email': self.custom_user_2.email,
            'diet_profile_id': self.diet_profile_2.id,
            'cart_id': -1,
        }

        response = self.client.post('/auth/link-data/', data, format='json')
        self.assertEqual(json.loads(response.content).get('message'), 'Cart is not found.')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_login_user_succeed(self):
        data = {
            'email': 'email@email.com',
            'password': 'abc',
            'role': 'client',
        }

        response = self.client.post('/auth/user-login/', data, format='json')
        json_response = json.loads(response.content)
        self.assertIn('access_token', json_response)
        self.assertIn('refresh_token', json_response)
        self.assertIn('user', json_response)
        self.assertIn('id', json_response.get('user'))
        self.assertIn('role', json_response.get('user'))
        self.assertEquals(json_response.get('user').get('name'), 'tes')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_login_user_failed_because_non_existing_email(self):
        data = {
            'email': 'email123123123@email.com',
            'password': 'abc',
            'role': 'client'
        }

        response = self.client.post('/auth/user-login/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_post_login_user_failed_because_invalid_role(self):
        data = {
            'email': 'email@email.com',
            'password': 'abc',
            'role': 'artist'
        }

        response = self.client.post('/auth/user-login/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_login_user_failed_because_invalid_email_format(self):
        data = {
            'email': 'email123123123@email',
            'password': 'abc',
            'role': 'client'
        }

        response = self.client.post('/auth/user-login/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_post_login_user_failed_because_invalid_password(self):
        data = {
            'email': 'email@email.com',
            'password': 'abc1234',
            'role': 'client'
        }

        response = self.client.post('/auth/user-login/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_post_login_user_failed_because_inactive_account(self):
        data = {
            'email': 'inactive@gmail.com',
            'password': 'abc',
            'role': 'client'
        }
        response = self.client.post('/auth/user-login/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_login_user_failed_because_no_role_in_payload(self):
        data = {
            'email': 'email@email.com',
            'password': 'abc',
        }
        response = self.client.post('/auth/user-login/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_login_serializer_to_do_nothing(self):
        serializer = CustomLoginSerializer()
        self.assertEqual(serializer.create({}), None)
    
    def test_update_login_serializer_to_do_nothing(self):
        serializer = CustomLoginSerializer()
        self.assertEqual(serializer.update(None, {}), None)

@patch('authentication.google_utils.requests.get')
@patch('authentication.google_utils.json.loads')
class TestValidateGoogleToken(TestCase):
    def test_validate_access_token_succeed(self, mock_json_loads, mock_get):
        mock_get.return_value.text = "ABCDEFGH"
        mock_json_loads.return_value = {"name": "mock", "email": 'mock_user@email.com'}
        success, google_data = validate_google_token("ABCDEFGH")

        self.assertTrue(success)
        self.assertEqual('mock_user@email.com', google_data.get('email'))
        self.assertEqual('mock', google_data.get('name'))

    def test_validate_access_token_failed(self, mock_json_loads, mock_get):
        mock_get.return_value.text = "123123123"
        mock_json_loads.return_value = {
            'error': 'invalid_token',
            'message': 'wrong google token / this google token is already expired.'
        }
        success, google_data = validate_google_token("123123123")

        self.assertFalse(success)
        self.assertEqual(google_data.status_code, status.HTTP_400_BAD_REQUEST)

@patch('authentication.views.validate_google_token')
class TestGoogleLogin(TestCase):
    def test_google_login_succeed(self, mock_validate_google_token):
        mock_validate_google_token.return_value = True, {"name": "tes", "email": 'email@email.com'}

        data = {'access_token': 'QWERTY'}
        response = self.client.post('/auth/google/', data, format='json')
        json_response = json.loads(response.content)
        self.assertIn('access_token', json_response)
        self.assertIn('refresh_token', json_response)
        self.assertIn('user', json_response)
        self.assertIn('role', json_response.get('user'))
        self.assertIn('name', json_response.get('user'))
        self.assertIn('email', json_response.get('user'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_google_login_failed(self, mock_validate_google_token):
        content = {'message': 'wrong google token / this google token is already expired.'}
        mock_validate_google_token.return_value = False, JsonResponse(content,
            status=status.HTTP_400_BAD_REQUEST)
        data = {'access_token': 'sdasdasdad'}
        response = self.client.post('/auth/google/', data, format='json')
        json_response = json.loads(response.content)
        self.assertIn('message', json_response)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
