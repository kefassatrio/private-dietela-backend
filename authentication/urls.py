from rest_framework import routers
from django.urls import path, include
from .views import LinkDataViewSet, GoogleView, CustomLoginView

router = routers.SimpleRouter()
router.register('auth/link-data', LinkDataViewSet, \
    basename="LinkData")

urlpatterns = [
    path('auth/registration/', include('dj_rest_auth.registration.urls')),
    path('auth/', include('dj_rest_auth.urls')),
    path('auth/user-login/', CustomLoginView.as_view(), name='custom_user_login'),
    path('auth/google/', GoogleView.as_view(), name='google'),
]

urlpatterns += router.urls
