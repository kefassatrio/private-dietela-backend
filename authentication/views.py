from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from dj_rest_auth.views import LoginView
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password
from dietela_quiz.models import DietProfile
from dietela_quiz.serializers import DietProfileSerializer
from payment.models import Cart
from payment.serializers import CartSerializer
from .models import CustomUser
from .serializers import CustomUserDetailsSerializer, CustomLoginSerializer
from .google_utils import validate_google_token

class CustomLoginView(LoginView):
    serializer_class = CustomLoginSerializer


class LinkDataViewSet(viewsets.ViewSet):

    @staticmethod
    def create(request):
        email = request.data['email']
        diet_profile_id = request.data['diet_profile_id']
        cart_id = request.data['cart_id']

        try:
            user = CustomUser.objects.get(email=email)
        except CustomUser.DoesNotExist:
            return Response({'message': 'User is not found.'}, status=status.HTTP_400_BAD_REQUEST)

        try:
            diet_profile = DietProfile.objects.get(id=diet_profile_id)
        except DietProfile.DoesNotExist:
            return Response({'message': 'Diet profile is not found.'}, \
                status=status.HTTP_400_BAD_REQUEST)

        try:
            cart = Cart.objects.get(id=cart_id)
        except Cart.DoesNotExist:
            return Response({'message': 'Cart is not found.'}, status=status.HTTP_400_BAD_REQUEST)

        user.diet_profile = diet_profile
        user.save()

        cart.user = user
        cart.save()

        return Response({\
            'user': CustomUserDetailsSerializer(user).data,\
            'diet_profile': DietProfileSerializer(diet_profile).data, \
            'cart': CartSerializer(cart).data \
        }, status=status.HTTP_200_OK)


class GoogleView(APIView):
    def post(self, request):
        success, google_data = validate_google_token(request.data.get('access_token'))
        if not success:
            return google_data
        try:
            user = CustomUser.objects.get(email=
                google_data.get('email')
            )
        except CustomUser.DoesNotExist:
            user = CustomUser.objects.create_user(
                email=google_data.get('email'),
                name=google_data.get('name'),
                # random default password
                password=make_password(BaseUserManager().make_random_password()),
            )
        token = RefreshToken.for_user(user)  # generate token
        response = {}
        response['user'] = CustomUserDetailsSerializer(user).data
        response['access_token'] = str(token.access_token)
        response['refresh_token'] = str(token)
        return Response(response)
