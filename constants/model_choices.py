from django.db import models

class Gender(models.IntegerChoices):
    MALE = 1, 'Laki-laki'
    FEMALE = 2, 'Perempuan'

class SpecialCondition(models.IntegerChoices):
    NONE = 1, 'Tidak ada'
    PREGNANT_1 = 2, 'Hamil Trimester 1'
    PREGNANT_2 = 3, 'Hamil Trimester 2'
    PREGNANT_3 = 4, 'Hamil Trimester 3'
    MILK_1 = 5, 'Menyusui Bayi 0-6 Bulan'
    MILK_2 = 6, 'Menyusui Bayi 7-12 Bulan'

class BodyActivity(models.IntegerChoices):
    NO_SPORT = 1, 'Lebih banyak duduk dan tidak rutin olahraga'
    WALK = 2, 'Banyak jalan, banyak bergerak, tetapi tidak rutin olahraga'
    ROUTINE_1 = 3, 'Olahraga rutin 30-45 menit per hari'
    ROUTINE_2 = 4, 'Olahraga rutin 45-60 menit per hari'
    ROUTINE_3 = 5, 'Olahraga rutin lebih dari 60 menit per hari'

class PiecesInOneDay(models.IntegerChoices):
    NO_PIECE = 1, '0 potong'
    ONE_PIECE = 2, '1 potong'
    TWO_PIECES = 3, '2 potong'
    THREE_PIECES = 4, '3 potong'
    MORE_PIECES = 5, 'Lebih dari 3 potong'

class DrinksInOneDay(models.IntegerChoices):
    NO_GLASS = 1, '0 gelas'
    ONE_GLASS = 2, '1 gelas'
    TWO_GLASSES = 3, '2 gelas'
    THREE_GLASSES = 4, '3 gelas'
    MORE_GLASSES = 5, 'Lebih dari 3 gelas'

class PortionInOneDay(models.IntegerChoices):
    NO_PORTION = 1, '0 porsi'
    ONE_PORTION = 2, '1 porsi'
    TWO_PORTIONS = 3, '2 porsi'
    THREE_PORTIONS = 4, '3 porsi'
    MORE_PORTIONS = 5, 'Lebih dari 3 porsi'

class LargeMealInOneDay(models.IntegerChoices):
    ONE_TIME = 1, '1 kali'
    TWO_TIMES = 2, '2 kali'
    THREE_TIMES = 3, '3 kali'
    MORE_TIMES = 4, 'Lebih dari 3 kali'

class SnacksInOneDay(models.IntegerChoices):
    NONE = 1, 'Tidak pernah'
    ONE_TIME = 2, '1 kali'
    TWO_TIMES = 3, '2 kali'
    THREE_TIMES = 4, '3 kali'
    MORE_TIMES = 5, 'Lebih dari 3 kali'

class Breakfast(models.IntegerChoices):
    NONE = 1, 'Tidak pernah makan pagi'
    LIGHT = 2, 'Makanan yang ringan saja'
    HEAVY = 3, 'Makanan yang sampai mengenyangkan perut'

class CurrentCondition(models.IntegerChoices):
    CONDITION_1 = 1, 'Saya belum tertarik melakukan diet dan gaya hidup sehat dan tidak tahu kondisi gizi serta kesehatan saya'
    CONDITION_2 = 2, 'Saya belum tertarik melakukan diet dan gaya hidup sehat meskipun saya tahu kondisi gizi dan kesehatan saya'
    CONDITION_3 = 3, 'Saya mulai tertarik melakukan diet dan gaya hidup sehat meskipun saya tidak tahu kondisi gizi dan kesehatan saya'
    CONDITION_4 = 4, 'Saya mulai tertarik melakukan diet dan gaya hidup sehat dan tahu kondisi gizi dan kesehatan saya'
    CONDITION_5 = 5, 'Saya sudah tahu manfaat diet dan gaya hidup sehat namun belum tahu harus mulai menerapkannya dari mana'
    CONDITION_6 = 6, 'Saya sudah pernah merubah diet dan gaya hidup menjadi lebih sehat tetapi tidak bertahan lama (dilakukan kurang dari 6 bulan)'
    CONDITION_7 = 7, 'Saya sudah pernah berhasil menjalani diet dan gaya hidup sehat selama lebih dari 6 bulan dan ingin melakukannya lagi'

class ProblemToSolve(models.IntegerChoices):
    PROBLEM_1 = 1, 'Turun berat badan dengan target yang realistis'
    PROBLEM_2 = 2, 'Naik berat badan dengan target yang realistis'
    PROBLEM_3 = 3, 'Berat badan yang bertahan permanen, menjaga agar berat badan tidak naik, anti yo-yo diet'
    PROBLEM_4 = 4, 'Butuh pengaturan gizi Ibu Hamil'
    PROBLEM_5 = 5, 'Butuh pengaturan gizi Ibu Menyusui'
    PROBLEM_6 = 6, 'Menyiapkan tubuh untuk mencapai kesuburan dan persiapan kehamilan'
    PROBLEM_7 = 7, 'Mengatur gula darah tinggi (Pre-diabetes), hiperlipidemia (kolesterol), hipertensi (tekanan darah tinggi)'
    PROBLEM_8 = 8, 'Capai pola makan sehat dan tetap enjoy menikmati makanan yang disukai'
    PROBLEM_9 = 9, 'Transisi vegan'
    PROBLEM_10 = 10, ' Butuh pengaturan gizi pada anak-anak atau remaja'

class HealthProblem(models.IntegerChoices):
    PROBLEM_1 = 1, 'Tidak ada yang perlu dikhawatirkan'
    PROBLEM_2 = 2, 'Diabetes Tipe 2'
    PROBLEM_3 = 3, 'Diabetes Tipe 1'
    PROBLEM_4 = 4, 'PCOS (Polycystic Ovary Syndrome)'
    PROBLEM_5 = 5, 'Kolesterol tinggi'
    PROBLEM_6 = 6, 'Asam urat tinggi'
    PROBLEM_7 = 7, 'Tekanan darah tinggi'
    PROBLEM_8 = 8, 'Gula darah tinggi'
    PROBLEM_9 = 9, 'Kanker'
    PROBLEM_10 = 10, 'HIV/AIDS'
    PROBLEM_11 = 11, 'Maag/GERD/Dispepsia/Gangguan lambung'
    PROBLEM_12 = 12, 'Penyintas Kanker'
    PROBLEM_13 = 13, 'Penyakit Kronis Lainnya'
