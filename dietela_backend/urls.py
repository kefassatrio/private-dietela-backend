from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('nutritionists.urls')),
    path('', include('dietela_quiz.urls')),
    path('', include('dietela_program.urls')),
    path('', include('payment.urls')),
    path('', include('authentication.urls')),
]
