from django.apps import AppConfig


class DietelaProgramConfig(AppConfig):
    name = 'dietela_program'
