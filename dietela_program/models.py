from django.db import models


class DietelaProgram(models.Model):
    unique_code = models.CharField(default="", max_length=255)
    name = models.CharField(max_length=255)
    price = models.FloatField()

    def __str__(self):
        return f"{self.unique_code} - {self.name}"
