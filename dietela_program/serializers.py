from rest_framework import serializers
from .models import DietelaProgram


class DietelaProgramSerializer(serializers.ModelSerializer):

    class Meta:
        model = DietelaProgram
        fields = "__all__"
