from rest_framework.test import APITestCase
from rest_framework import status
from .models import DietelaProgram
from .serializers import DietelaProgramSerializer


class DietelaProgramTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.BASE_URL = "/dietela-programs/"

        cls.dietela_program_1 = DietelaProgram.objects.create(
            unique_code="PRG1",
            name="Program 1",
            price=350000.00,
        )
        cls.dietela_program_2 = DietelaProgram.objects.create(
            unique_code="PRG2",
            name="Program 2",
            price=300000.00,
        )

    def test_get_all_dietela_programs(self):
        response = self.client.get(self.BASE_URL)

        dietela_programs = DietelaProgram.objects.all()
        serializer = DietelaProgramSerializer(dietela_programs, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_dietela_program(self):
        url = f"{self.BASE_URL}{self.dietela_program_1.pk}/"
        response = self.client.get(url)

        dietela_program = DietelaProgram.objects.get(
            pk=self.dietela_program_1.pk)
        serializer = DietelaProgramSerializer(dietela_program)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_dietela_program(self):
        url = f"{self.BASE_URL}1303/"
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_dietela_program_string(self):
        self.assertEqual(str(self.dietela_program_1), 'PRG1 - Program 1')
