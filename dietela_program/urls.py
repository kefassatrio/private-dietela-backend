from rest_framework import routers
from .views import DietelaProgramViewSet

router = routers.SimpleRouter()
router.register('dietela-programs', DietelaProgramViewSet)

urlpatterns = []

urlpatterns += router.urls
