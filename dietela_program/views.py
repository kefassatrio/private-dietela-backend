from rest_framework import viewsets
from .serializers import DietelaProgramSerializer
from .models import DietelaProgram


class DietelaProgramViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = DietelaProgramSerializer
    queryset = DietelaProgram.objects.all()
