from django.contrib import admin
from .models import DietProfile

admin.site.register(DietProfile)
