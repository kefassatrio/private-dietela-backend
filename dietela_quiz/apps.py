from django.apps import AppConfig


class DietelaQuizConfig(AppConfig):
    name = 'dietela_quiz'
