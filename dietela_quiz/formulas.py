from constants.quiz_constants import (
    BreakfastReponse,
    PhysicalActivityResponse,
    ProgramRecommendationResponse,
    BodyMassConstants,
    VegetableAndFruitSufficiencyResponse,
    VegetableAndFruitDietRecommendation,
    SugarSaltFatProblemResponse,
    SugarSaltFatDietRecommendation,
    LargeMealDietRecommendation,
    SnacksDietRecommendation,
)


def get_body_mass_index(weight, height):
    if (weight < 0) or (height < 0):
        raise ValueError("Invalid input for body mass index!")
    return weight / ((height / 100)**2)


def get_nutrition_status(body_mass_index):
    if body_mass_index < 18.50:
        return BodyMassConstants.UNDERWEIGHT
    if body_mass_index <= 22.99:
        return BodyMassConstants.NORMAL
    if body_mass_index <= 25:
        return BodyMassConstants.PREOBESITY
    if body_mass_index <= 29.99:
        return BodyMassConstants.OBESITY1
    return BodyMassConstants.OBESITY2


def get_ideal_weight_range(height):
    if height < 0:
        raise ValueError("Invalid input for ideal weight range!")
    return {'min': 19 * ((height / 100)**2), 'max': 23 * ((height / 100)**2)}


def get_basic_energy_needs(gender, weight, height, age):
    if (gender > 2) or (gender < 1) or (
            weight < 0) or (height < 0) or (age < 0):
        raise ValueError("Invalid input for basic energy needs!")
    if gender == 1:
        return (10 * weight) + (6.25 * height - 5 * age) + 5
    return (10 * weight) + (6.25 * height - 5 * age) - 161


def get_physical_activity_percentage(body_activity):
    if (body_activity > 5) or (body_activity < 1):
        raise ValueError("Invalid choice number!")
    physical_activity_percentage_switch = {
        1: 0.20,
        2: 0.20,
        3: 0.30,
        4: 0.35,
        5: 0.40
    }
    return physical_activity_percentage_switch.get(body_activity)


def get_daily_energy_needs(
        special_condition,
        body_mass,
        basic_energy_needs,
        physical_activity_percentage):
    if special_condition == 2:
        return basic_energy_needs + \
            (physical_activity_percentage * basic_energy_needs)
    if special_condition == 3:
        return basic_energy_needs + \
            (physical_activity_percentage * basic_energy_needs) + 340
    if special_condition == 4:
        return basic_energy_needs + \
            (physical_activity_percentage * basic_energy_needs) + 452
    if special_condition == 5:
        return basic_energy_needs + \
            (physical_activity_percentage * basic_energy_needs) + 330
    if special_condition == 6:
        return basic_energy_needs + \
            (physical_activity_percentage * basic_energy_needs) + 400
    if body_mass in (BodyMassConstants.OBESITY1, BodyMassConstants.OBESITY2):
        return basic_energy_needs
    if body_mass == BodyMassConstants.UNDERWEIGHT:
        return basic_energy_needs + \
            (physical_activity_percentage * basic_energy_needs) + (0.1 * basic_energy_needs)
    return basic_energy_needs + \
        (physical_activity_percentage * basic_energy_needs)


def get_breakfast_recommendation(breakfast_type):
    if breakfast_type == 1:
        return BreakfastReponse.NO_BREAKFAST
    if breakfast_type == 2:
        return BreakfastReponse.MED_BREAKFAST
    return BreakfastReponse.HI_BREAKFAST


def get_physical_activity_recommendation(physical_activity_frequency):
    if physical_activity_frequency == 1:
        return PhysicalActivityResponse.LEVEL1_ACTIVITY
    if physical_activity_frequency == 2:
        return PhysicalActivityResponse.LEVEL2_ACTIVITY
    if physical_activity_frequency == 3:
        return PhysicalActivityResponse.LEVEL3_ACTIVITY
    if physical_activity_frequency == 4:
        return PhysicalActivityResponse.LEVEL4_ACTIVITY
    return PhysicalActivityResponse.LEVEL5_ACTIVITY


def get_energy_needed_per_dine(breakfast_type, daily_energy_needs):
    energy_dictionary = {}
    if 0 < breakfast_type < 3:
        energy_dictionary['breakfast'] = daily_energy_needs * 0.1
        energy_dictionary['morning_snack'] = daily_energy_needs * 0.15
        energy_dictionary['lunch'] = daily_energy_needs * 0.3
        energy_dictionary['afternoon_snack'] = daily_energy_needs * 0.15
        energy_dictionary['dinner'] = daily_energy_needs * 0.3
    elif breakfast_type == 3:
        energy_dictionary['breakfast'] = daily_energy_needs * 0.2
        energy_dictionary['morning_snack'] = daily_energy_needs * 0.1
        energy_dictionary['lunch'] = daily_energy_needs * 0.3
        energy_dictionary['afternoon_snack'] = daily_energy_needs * 0.1
        energy_dictionary['dinner'] = daily_energy_needs * 0.3
    else:
        return energy_dictionary
    return energy_dictionary

def mapping_current_condition(item_18):
    current_condition = 0
    if item_18 <= 2:
        current_condition = 200
    elif item_18 == 3:
        current_condition = 10
    else:
        current_condition = 30
    return current_condition

def mapping_problem_to_solve(item_19):
    problem_to_solve = 0
    if item_19 <= 3:
        problem_to_solve = 55
    elif 4 <= item_19 <= 6:
        problem_to_solve = 7
    else:
        problem_to_solve = 115
    return problem_to_solve

def mapping_health_problem(item_20):
    health_problem = 0
    for i in item_20:
        if i >= 11:
            health_problem += 10
        elif i == 4:
            health_problem += 7
        elif i == 1:
            health_problem += 5
        else:
            health_problem += 30
    return health_problem

def get_program_recommendation(item_18, item_19, item_20):
    current_condition = mapping_current_condition(item_18)
    problem_to_solve = mapping_problem_to_solve(item_19)
    health_problem = mapping_health_problem(item_20)

    score = current_condition + problem_to_solve + health_problem
    programs_dictionary = {}

    if (22 <= score <= 30) or (70 <= score <= 79):
        programs_dictionary['priority_1'] = ProgramRecommendationResponse.BABY_1
        programs_dictionary['priority_2'] = ProgramRecommendationResponse.TRIAL
    elif 31 <= score <= 69:
        programs_dictionary['priority_1'] = ProgramRecommendationResponse.BABY_3
        programs_dictionary['priority_2'] = ProgramRecommendationResponse.BABY_1
    elif 80 <= score <= 99:
        programs_dictionary['priority_1'] = ProgramRecommendationResponse.GOALS_3
        programs_dictionary['priority_2'] = ProgramRecommendationResponse.GOALS_1
    elif 100 <= score <= 119:
        programs_dictionary['priority_1'] = ProgramRecommendationResponse.GOALS_6
        programs_dictionary['priority_2'] = ProgramRecommendationResponse.GOALS_3
    elif 120 <= score <= 150:
        programs_dictionary['priority_1'] = ProgramRecommendationResponse.BALANCED_1
        programs_dictionary['priority_2'] = ProgramRecommendationResponse.TRIAL
    elif 151 <= score <= 159:
        programs_dictionary['priority_1'] = ProgramRecommendationResponse.BALANCED_3
        programs_dictionary['priority_2'] = ProgramRecommendationResponse.BALANCED_1
    elif 160 <= score <= 179:
        programs_dictionary['priority_1'] = ProgramRecommendationResponse.BALANCED_6
        programs_dictionary['priority_2'] = ProgramRecommendationResponse.BALANCED_3
    else:
        programs_dictionary['priority_1'] = ProgramRecommendationResponse.TRIAL
        programs_dictionary['priority_2'] = None
    return programs_dictionary


def get_calculate_specific_nutrition_needs(
        percentage, daily_energy_needs, denominator):
    return ((percentage / 100) * daily_energy_needs) / denominator


def get_protein_needs(
        special_condition_choice,
        body_mass_index,
        daily_energy_needs):
    client_nutrition_status = get_nutrition_status(body_mass_index)
    if (
        special_condition_choice != 1) or (
        client_nutrition_status == BodyMassConstants.UNDERWEIGHT) or (
            client_nutrition_status == BodyMassConstants.NORMAL) or (
                client_nutrition_status == BodyMassConstants.PREOBESITY):
        return get_calculate_specific_nutrition_needs(13, daily_energy_needs, 4)
    return get_calculate_specific_nutrition_needs(18, daily_energy_needs, 4)


def get_fat_needs(special_condition_choice, body_mass_index, daily_energy_needs):
    client_nutrition_status = get_nutrition_status(body_mass_index)
    if (
        special_condition_choice != 1) or (
        client_nutrition_status == BodyMassConstants.UNDERWEIGHT) or (
            client_nutrition_status == BodyMassConstants.NORMAL) or (
                client_nutrition_status == BodyMassConstants.PREOBESITY):
        return get_calculate_specific_nutrition_needs(30, daily_energy_needs, 9)
    return get_calculate_specific_nutrition_needs(27, daily_energy_needs, 9)


def get_carbohydrate_needs(
        special_condition_choice,
        body_mass_index,
        daily_energy_needs):
    client_nutrition_status = get_nutrition_status(body_mass_index)
    if (
        special_condition_choice != 1) or (
        client_nutrition_status == BodyMassConstants.UNDERWEIGHT) or (
            client_nutrition_status == BodyMassConstants.NORMAL) or (
                client_nutrition_status == BodyMassConstants.PREOBESITY):
        return get_calculate_specific_nutrition_needs(57, daily_energy_needs, 4)
    return get_calculate_specific_nutrition_needs(55, daily_energy_needs, 4)


def get_fiber_needs():
    return 25


def get_food_consumed_score(choice):
    return choice - 1


def get_total_vegetable_and_fruit_score(vegetables_in_one_day, fruits_in_one_day):
    if (vegetables_in_one_day < 1) or (vegetables_in_one_day > 5):
        raise ValueError(f"{vegetables_in_one_day}: Invalid vegetable choice number!")

    if (fruits_in_one_day < 1) or (fruits_in_one_day > 5):
        raise ValueError(f"{fruits_in_one_day}: Invalid fruit choice number!")

    vegetable_score = get_food_consumed_score(vegetables_in_one_day)
    fruit_score = get_food_consumed_score(fruits_in_one_day)

    return vegetable_score + fruit_score


def get_vegetable_and_fruit_sufficiency_response(
        total_vegetable_and_fruit_score):
    if (total_vegetable_and_fruit_score < 0) or (
            total_vegetable_and_fruit_score > 8):
        raise ValueError(f"{total_vegetable_and_fruit_score}: \
            Invalid total vegetable and fruit score!")

    if 0 <= total_vegetable_and_fruit_score <= 4:
        return VegetableAndFruitSufficiencyResponse.LACKING
    return VegetableAndFruitSufficiencyResponse.ENOUGH


def get_vegetable_and_fruit_diet_recommendation(
        vegetable_and_fruit_suffiency_response):
    if (vegetable_and_fruit_suffiency_response ==
            VegetableAndFruitSufficiencyResponse.LACKING):
        return VegetableAndFruitDietRecommendation.LACKING
    return VegetableAndFruitDietRecommendation.ENOUGH


def get_total_sugar_salt_fat_score(
        fried_food_choice,
        sweet_snacks_choice,
        sweet_drinks_choice,
        packaged_food_choice):
    if (fried_food_choice < 1) or (fried_food_choice > 5):
        raise ValueError(f"{fried_food_choice}: Invalid fried food choice number!")

    if (sweet_snacks_choice < 1) or (sweet_snacks_choice > 5):
        raise ValueError(f"{sweet_snacks_choice}: Invalid sweet snacks choice number!")

    if (sweet_drinks_choice < 1) or (sweet_drinks_choice > 5):
        raise ValueError(f"{sweet_drinks_choice}: Invalid sweet drinks choice number!")

    if (packaged_food_choice < 1) or (packaged_food_choice > 5):
        raise ValueError(f"{packaged_food_choice}: Invalid packaged food choice number!")

    fried_food_score = get_food_consumed_score(fried_food_choice)
    sweet_snacks_score = get_food_consumed_score(sweet_snacks_choice)
    sweet_drinks_score = get_food_consumed_score(sweet_drinks_choice)
    packaged_food_score = get_food_consumed_score(packaged_food_choice)

    return fried_food_score + sweet_snacks_score + \
        sweet_drinks_score + packaged_food_score


def get_sugar_salt_fat_problem_response(total_sugar_salt_fat_score):
    if (total_sugar_salt_fat_score < 0) or (total_sugar_salt_fat_score > 16):
        raise ValueError(f"{total_sugar_salt_fat_score}: \
            Invalid total sugar, salt, fat score!")

    if 0 <= total_sugar_salt_fat_score <= 7:
        return SugarSaltFatProblemResponse.CONTROLLED
    return SugarSaltFatProblemResponse.EXCESSIVE


def get_sugar_salt_fat_diet_recommendation(sugar_salt_fat_problem_response):
    if (sugar_salt_fat_problem_response ==
            SugarSaltFatProblemResponse.CONTROLLED):
        return SugarSaltFatDietRecommendation.CONTROLLED
    return SugarSaltFatDietRecommendation.EXCESSIVE


def get_large_meal_diet_recommendation(large_meal_in_one_day):
    if (large_meal_in_one_day < 1) or (large_meal_in_one_day > 4):
        raise ValueError(f"{large_meal_in_one_day}: Invalid large meal choice number!")

    if large_meal_in_one_day == 1:
        return LargeMealDietRecommendation.ONCE_A_DAY
    if large_meal_in_one_day == 2:
        return LargeMealDietRecommendation.TWICE_A_DAY
    if large_meal_in_one_day == 3:
        return LargeMealDietRecommendation.THRICE_A_DAY
    return LargeMealDietRecommendation.MORE_THAN_THRICE_A_DAY


def get_snacks_diet_recommendation(snacks_in_one_day):
    if (snacks_in_one_day < 1) or (snacks_in_one_day > 5):
        raise ValueError(f"{snacks_in_one_day}: Invalid snacks choice number!")

    if snacks_in_one_day == 1:
        return SnacksDietRecommendation.NO_SNACK
    if snacks_in_one_day == 2:
        return SnacksDietRecommendation.ONCE_A_DAY
    if snacks_in_one_day == 3:
        return SnacksDietRecommendation.TWICE_A_DAY
    if snacks_in_one_day == 4:
        return SnacksDietRecommendation.THRICE_A_DAY
    return SnacksDietRecommendation.MORE_THAN_THRICE_A_DAY
