from django.db import models
from multiselectfield import MultiSelectField
from constants.model_choices import (
    Gender,
    SpecialCondition,
    BodyActivity,
    PiecesInOneDay,
    DrinksInOneDay,
    PortionInOneDay,
    LargeMealInOneDay,
    SnacksInOneDay,
    Breakfast,
    CurrentCondition,
    ProblemToSolve,
    HealthProblem
)


class DietProfile(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    age = models.PositiveIntegerField()
    weight = models.PositiveIntegerField()
    height = models.PositiveIntegerField()
    gender = models.IntegerField(choices=Gender.choices)
    special_condition = models.IntegerField(
        choices=SpecialCondition.choices)
    body_activity = models.IntegerField(
        choices=BodyActivity.choices)
    vegetables_in_one_day = models.IntegerField(
        choices=PortionInOneDay.choices)
    fruits_in_one_day = models.IntegerField(
        choices=PortionInOneDay.choices)
    fried_food_in_one_day = models.IntegerField(
        choices=PiecesInOneDay.choices)
    sweet_snacks_in_one_day = models.IntegerField(
        choices=PiecesInOneDay.choices)
    sweet_drinks_in_one_day = models.IntegerField(
        choices=DrinksInOneDay.choices)
    packaged_food_in_one_day = models.IntegerField(
        choices=PortionInOneDay.choices)
    large_meal_in_one_day = models.IntegerField(
        choices=LargeMealInOneDay.choices)
    snacks_in_one_day = models.IntegerField(
        choices=SnacksInOneDay.choices)
    breakfast_type = models.IntegerField(
        choices=Breakfast.choices)
    current_condition = models.IntegerField(
        choices=CurrentCondition.choices)
    problem_to_solve = models.IntegerField(
        choices=ProblemToSolve.choices)
    health_problem = MultiSelectField(
        choices=HealthProblem.choices)

    def __str__(self):
        return f"{self.name} - {self.email}"


class QuizResult(models.Model):
    diet_profile = models.OneToOneField(
        DietProfile,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name="quiz_result"
    )
    age = models.PositiveIntegerField()
    weight = models.PositiveIntegerField()
    height = models.PositiveIntegerField()
    gender = models.IntegerField(choices=Gender.choices)
    body_mass_index = models.PositiveIntegerField()
    nutrition_status = models.CharField(max_length=100)
    ideal_weight_range = models.JSONField()
    daily_energy_needs = models.FloatField()
    daily_nutrition_needs = models.JSONField()
    vegetable_and_fruit_sufficiency = models.CharField(max_length=100)
    vegetable_and_fruit_diet_recommendation = models.TextField()
    sugar_salt_fat_problem = models.CharField(max_length=100)
    sugar_salt_fat_diet_recommendation = models.TextField()
    large_meal_diet_recommendation = models.TextField()
    snacks_diet_recommendation = models.TextField()
    breakfast_recommendation = models.TextField()
    energy_needed_per_dine = models.JSONField()
    physical_activity_recommendation = models.TextField()
    program_recommendation = models.JSONField()

    def __str__(self):
        return str(self.diet_profile)
