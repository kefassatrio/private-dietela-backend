from rest_framework import fields, serializers
from constants.model_choices import HealthProblem
from .models import DietProfile, QuizResult


class DietProfileSerializer(serializers.ModelSerializer):
    health_problem = fields.MultipleChoiceField(
        choices=HealthProblem.choices)

    class Meta:
        model = DietProfile
        fields = "__all__"


class QuizResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuizResult
        fields = "__all__"
