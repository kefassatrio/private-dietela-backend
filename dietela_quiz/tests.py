import json
from rest_framework.test import APITestCase
from rest_framework import status
from django.test import TestCase
from constants.quiz_constants import (
    BreakfastReponse,
    PhysicalActivityResponse,
    ProgramRecommendationResponse,
    BodyMassConstants,
    VegetableAndFruitSufficiencyResponse,
    VegetableAndFruitDietRecommendation,
    SugarSaltFatProblemResponse,
    SugarSaltFatDietRecommendation,
    LargeMealDietRecommendation,
    SnacksDietRecommendation,
)
from .models import DietProfile, QuizResult
from .formulas import (
    get_body_mass_index,
    get_nutrition_status,
    get_ideal_weight_range,
    get_basic_energy_needs,
    get_physical_activity_percentage,
    get_daily_energy_needs,
    get_breakfast_recommendation,
    get_energy_needed_per_dine,
    get_physical_activity_recommendation,
    get_program_recommendation,
    get_calculate_specific_nutrition_needs,
    get_protein_needs,
    get_fat_needs,
    get_carbohydrate_needs,
    get_fiber_needs,
    get_food_consumed_score,
    get_total_vegetable_and_fruit_score,
    get_vegetable_and_fruit_sufficiency_response,
    get_vegetable_and_fruit_diet_recommendation,
    get_total_sugar_salt_fat_score,
    get_sugar_salt_fat_problem_response,
    get_sugar_salt_fat_diet_recommendation,
    get_large_meal_diet_recommendation,
    get_snacks_diet_recommendation,
)


class DietelaQuizTests(APITestCase):

    def setUp(self):
        self.diet_profile = DietProfile.objects.create(
            name="test",
            email="test@test.com",
            age=20,
            weight=61,
            height=172,
            gender=2,
            special_condition=1,
            body_activity=1,
            vegetables_in_one_day=1,
            fruits_in_one_day=1,
            fried_food_in_one_day=1,
            sweet_snacks_in_one_day=1,
            sweet_drinks_in_one_day=1,
            packaged_food_in_one_day=1,
            large_meal_in_one_day=1,
            snacks_in_one_day=1,
            breakfast_type=1,
            current_condition=1,
            problem_to_solve=1,
            health_problem=[2, 3])

    def test_diet_profile_saved(self):
        self.assertEqual(DietProfile.objects.count(), 1)

    def test_diet_profile_string(self):
        self.assertEqual(str(self.diet_profile), 'test - test@test.com')

    def test_get_diet_profile_api(self):
        response = self.client.get('/dietela-quiz/diet-profile/')
        self.assertEqual(json.loads(response.content)[0].get('name'), 'test')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_diet_profile_api(self):
        data = {
            'name': 'test2',
            'email': 'test2@test.com',
            'age': 20,
            'weight': 60,
            'height': 172,
            'gender': 2,
            'special_condition': 1,
            'body_activity': 1,
            'vegetables_in_one_day': 1,
            'fruits_in_one_day': 1,
            'fried_food_in_one_day': 1,
            'sweet_snacks_in_one_day': 1,
            'sweet_drinks_in_one_day': 1,
            'packaged_food_in_one_day': 1,
            'large_meal_in_one_day': 1,
            'snacks_in_one_day': 1,
            'breakfast_type': 1,
            'current_condition': 1,
            'problem_to_solve': 1,
            'health_problem': [2, 3]
        }
        response = self.client.post(
            '/dietela-quiz/diet-profile/', data, format='json')
        self.assertEqual(json.loads(response.content).get('name'), 'test2')
        self.assertEqual(json.loads(response.content).get('quiz_result').get('age'), 20)
        self.assertEqual(DietProfile.objects.count(), 2)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_auto_quiz_results_saved(self):
        data = {
            'name': 'test3',
            'email': 'test3@test.com',
            'age': 20,
            'weight': 60,
            'height': 172,
            'gender': 2,
            'special_condition': 1,
            'body_activity': 1,
            'vegetables_in_one_day': 1,
            'fruits_in_one_day': 1,
            'fried_food_in_one_day': 1,
            'sweet_snacks_in_one_day': 1,
            'sweet_drinks_in_one_day': 1,
            'packaged_food_in_one_day': 1,
            'large_meal_in_one_day': 1,
            'snacks_in_one_day': 1,
            'breakfast_type': 1,
            'current_condition': 1,
            'problem_to_solve': 1,
            'health_problem': [1]
        }
        self.client.post(
            '/dietela-quiz/diet-profile/', data, format='json')
        self.assertEqual(QuizResult.objects.filter(diet_profile__name='test3').count(), 1)

    def test_incomplete_post_diet_profile_api(self):
        '''
        Post without food or drink portion in a day,
        and without beakfast type, current condition,
        problem to solve, and health problem fields
        '''

        data = {
            'name': 'test2',
            'email': 'test2@test.com',
            'age': 20,
            'weight': 60,
            'height': 172,
            'gender': 2,
            'special_condition': 1,
            'body_activity': 1
        }
        response = self.client.post(
            '/dietela-quiz/diet-profile/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class FormulaTest(TestCase):

    def test_get_body_mass_index(self):
        weight = 61
        height = 172
        self.assertEqual(get_body_mass_index(weight, height),
                         weight / ((height / 100)**2))

    def test_invalid_input_get_body_mass_index(self):
        weight_1 = -1
        height_1 = 172
        weight_2 = 61
        height_2 = -1
        with self.assertRaises(ValueError):
            get_body_mass_index(weight_1, height_1)
        with self.assertRaises(ValueError):
            get_body_mass_index(weight_2, height_2)

    def test_get_nutrition_status(self):
        self.assertEqual(get_nutrition_status(18), BodyMassConstants.UNDERWEIGHT)
        self.assertEqual(get_nutrition_status(20.62), BodyMassConstants.NORMAL)
        self.assertEqual(get_nutrition_status(24), BodyMassConstants.PREOBESITY)
        self.assertEqual(get_nutrition_status(26), BodyMassConstants.OBESITY1)
        self.assertEqual(get_nutrition_status(32), BodyMassConstants.OBESITY2)

    def test_get_ideal_weight_range(self):
        height = 172
        self.assertEqual(get_ideal_weight_range(height),
                         {'min': 19 * ((height / 100)**2),
                          'max': 23 * ((height / 100)**2)})

    def test_invalid_input_get_ideal_weight_range(self):
        with self.assertRaises(ValueError):
            height = -1
            get_ideal_weight_range(height)

    def test_get_basic_energy_needs(self):
        gender_male = 1
        gender_female = 2
        weight = 61
        height = 172
        age = 20
        self.assertEqual(get_basic_energy_needs(
            gender_male, weight, height, age), (10 * weight) + (6.25 * height - 5 * age) + 5)
        self.assertEqual(get_basic_energy_needs(gender_female,
                                            weight,
                                            height,
                                            age),
                         (10 * weight) + (6.25 * height - 5 * age) - 161)

    def test_invalid_input_get_basic_energy_needs(self):
        gender_male = 0
        gender_female = 3
        weight = -1
        height = -1
        age = -1
        with self.assertRaises(ValueError):
            get_basic_energy_needs(gender_male, weight, height, age)
        with self.assertRaises(ValueError):
            get_basic_energy_needs(gender_female, weight, height, age)

    def test_get_physical_activity_percentage(self):
        self.assertEqual(get_physical_activity_percentage(1), 0.20)
        self.assertEqual(get_physical_activity_percentage(2), 0.20)
        self.assertEqual(get_physical_activity_percentage(3), 0.30)
        self.assertEqual(get_physical_activity_percentage(4), 0.35)
        self.assertEqual(get_physical_activity_percentage(5), 0.40)

    def test_invalid_input_get_physical_activity_percentage(self):
        with self.assertRaises(ValueError):
            get_physical_activity_percentage(0)
        with self.assertRaises(ValueError):
            get_physical_activity_percentage(6)

    def test_get_daily_energy_needs(self):
        basic_energy_needs = 100
        physical_activity_percentage = 0.2
        no_special_condition = 1
        pregnant_trisemester_1 = 2
        pregnant_trisemester_2 = 3
        pregnant_trisemester_3 = 4
        giving_baby_milk_1 = 5
        giving_baby_milk_2 = 6

        self.assertEqual(
            get_daily_energy_needs(
                no_special_condition,
                BodyMassConstants.UNDERWEIGHT,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs + (
                physical_activity_percentage * basic_energy_needs) + (
                0.1 * basic_energy_needs))
        self.assertEqual(
            get_daily_energy_needs(
                no_special_condition,
                BodyMassConstants.NORMAL,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs +
            (
                physical_activity_percentage *
                basic_energy_needs))
        self.assertEqual(
            get_daily_energy_needs(
                no_special_condition,
                BodyMassConstants.PREOBESITY,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs +
            (
                physical_activity_percentage *
                basic_energy_needs))

        self.assertEqual(
            get_daily_energy_needs(
                no_special_condition,
                BodyMassConstants.OBESITY1,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs)
        self.assertEqual(
            get_daily_energy_needs(
                no_special_condition,
                BodyMassConstants.OBESITY2,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs)

        self.assertEqual(
            get_daily_energy_needs(
                pregnant_trisemester_1,
                BodyMassConstants.NORMAL,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs +
            (
                physical_activity_percentage *
                basic_energy_needs))
        self.assertEqual(
            get_daily_energy_needs(
                pregnant_trisemester_2,
                BodyMassConstants.NORMAL,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs +
            (
                physical_activity_percentage *
                basic_energy_needs) +
            340)
        self.assertEqual(
            get_daily_energy_needs(
                pregnant_trisemester_3,
                BodyMassConstants.NORMAL,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs +
            (
                physical_activity_percentage *
                basic_energy_needs) +
            452)
        self.assertEqual(
            get_daily_energy_needs(
                giving_baby_milk_1,
                BodyMassConstants.NORMAL,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs +
            (
                physical_activity_percentage *
                basic_energy_needs) +
            330)
        self.assertEqual(
            get_daily_energy_needs(
                giving_baby_milk_2,
                BodyMassConstants.NORMAL,
                basic_energy_needs,
                physical_activity_percentage),
            basic_energy_needs +
            (
                physical_activity_percentage *
                basic_energy_needs) +
            400)

    def test_get_breakfast_recommendation(self):
        no_breakfast = 1
        med_breakfast = 2
        hi_breakfast = 3

        self.assertEqual(
            get_breakfast_recommendation(no_breakfast),
            BreakfastReponse.NO_BREAKFAST)
        self.assertEqual(
            get_breakfast_recommendation(med_breakfast),
            BreakfastReponse.MED_BREAKFAST)
        self.assertEqual(
            get_breakfast_recommendation(hi_breakfast),
            BreakfastReponse.HI_BREAKFAST)

    def test_get_physical_activity_recommendation(self):
        level1_activity = 1
        level2_activity = 2
        level3_activity = 3
        level4_activity = 4
        level5_activity = 5

        self.assertEqual(
            get_physical_activity_recommendation(level1_activity),
            PhysicalActivityResponse.LEVEL1_ACTIVITY)
        self.assertEqual(
            get_physical_activity_recommendation(level2_activity),
            PhysicalActivityResponse.LEVEL2_ACTIVITY)
        self.assertEqual(
            get_physical_activity_recommendation(level3_activity),
            PhysicalActivityResponse.LEVEL3_ACTIVITY)
        self.assertEqual(
            get_physical_activity_recommendation(level4_activity),
            PhysicalActivityResponse.LEVEL4_ACTIVITY)
        self.assertEqual(
            get_physical_activity_recommendation(level5_activity),
            PhysicalActivityResponse.LEVEL5_ACTIVITY)

    def test_get_energy_needed_per_dine(self):
        daily_energy_needs = 450
        never_breakfast = 1
        light_breakfast = 2
        heavy_breakfast = 3

        energy_needed_no_breakfast = get_energy_needed_per_dine(
            never_breakfast, daily_energy_needs)
        energy_needed_med_breakfast = get_energy_needed_per_dine(
            light_breakfast, daily_energy_needs)
        energy_needed_hi_breakfast = get_energy_needed_per_dine(
            heavy_breakfast, daily_energy_needs)
        energy_needed_error = get_energy_needed_per_dine(0, daily_energy_needs)

        self.assertEqual(
            energy_needed_no_breakfast.get('breakfast'),
            (daily_energy_needs * 0.1))
        self.assertEqual(energy_needed_no_breakfast.get(
            'morning_snack'), (daily_energy_needs * 0.15))
        self.assertEqual(
            energy_needed_no_breakfast.get('lunch'),
            (daily_energy_needs * 0.3))
        self.assertEqual(energy_needed_no_breakfast.get(
            'afternoon_snack'), (daily_energy_needs * 0.15))
        self.assertEqual(
            energy_needed_no_breakfast.get('dinner'),
            (daily_energy_needs * 0.3))

        self.assertEqual(
            energy_needed_no_breakfast.get('breakfast'),
            energy_needed_med_breakfast.get('breakfast'))
        self.assertEqual(
            energy_needed_no_breakfast.get('morning_snack'),
            energy_needed_med_breakfast.get('morning_snack'))
        self.assertEqual(
            energy_needed_no_breakfast.get('lunch'),
            energy_needed_med_breakfast.get('lunch'))
        self.assertEqual(
            energy_needed_no_breakfast.get('afternoon_snack'),
            energy_needed_med_breakfast.get('afternoon_snack'))
        self.assertEqual(
            energy_needed_no_breakfast.get('dinner'),
            energy_needed_med_breakfast.get('dinner'))

        self.assertEqual(
            energy_needed_hi_breakfast.get('breakfast'),
            (daily_energy_needs * 0.2))
        self.assertEqual(energy_needed_hi_breakfast.get(
            'morning_snack'), (daily_energy_needs * 0.1))
        self.assertEqual(
            energy_needed_hi_breakfast.get('lunch'),
            (daily_energy_needs * 0.3))
        self.assertEqual(energy_needed_hi_breakfast.get(
            'afternoon_snack'), (daily_energy_needs * 0.1))
        self.assertEqual(
            energy_needed_hi_breakfast.get('dinner'),
            (daily_energy_needs * 0.3))

        self.assertEqual(energy_needed_error, {})

    def test_get_program_recommendation(self):
        client_type_1 = get_program_recommendation(3, 6, [1])      # Score 22-30
        client_type_2 = get_program_recommendation(4, 4, [1])     # Score 31-69
        client_type_3 = get_program_recommendation(3, 1, [1])    # Score 70-79
        client_type_4 = get_program_recommendation(3, 1, [11,4])     # Score 80-99
        client_type_5 = get_program_recommendation(7, 1, [8])    # Score 100-119
        client_type_6 = get_program_recommendation(3, 7, [1])    # Score 120-150
        client_type_7 = get_program_recommendation(3, 10, [3])   # Score 151-159
        client_type_8 = get_program_recommendation(4, 1, [5,6,7])   # Score 160-179
        client_type_9 = get_program_recommendation(1, 1, [1])  # Score 180-345

        self.assertEqual(
            client_type_1.get('priority_1'),
            ProgramRecommendationResponse.BABY_1)
        self.assertEqual(
            client_type_1.get('priority_2'),
            ProgramRecommendationResponse.TRIAL)

        self.assertEqual(
            client_type_2.get('priority_1'),
            ProgramRecommendationResponse.BABY_3)
        self.assertEqual(
            client_type_2.get('priority_2'),
            ProgramRecommendationResponse.BABY_1)

        self.assertEqual(
            client_type_3.get('priority_1'),
            ProgramRecommendationResponse.BABY_1)
        self.assertEqual(
            client_type_3.get('priority_2'),
            ProgramRecommendationResponse.TRIAL)

        self.assertEqual(
            client_type_4.get('priority_1'),
            ProgramRecommendationResponse.GOALS_3)
        self.assertEqual(
            client_type_4.get('priority_2'),
            ProgramRecommendationResponse.GOALS_1)

        self.assertEqual(
            client_type_5.get('priority_1'),
            ProgramRecommendationResponse.GOALS_6)
        self.assertEqual(
            client_type_5.get('priority_2'),
            ProgramRecommendationResponse.GOALS_3)

        self.assertEqual(
            client_type_6.get('priority_1'),
            ProgramRecommendationResponse.BALANCED_1)
        self.assertEqual(
            client_type_6.get('priority_2'),
            ProgramRecommendationResponse.TRIAL)

        self.assertEqual(
            client_type_7.get('priority_1'),
            ProgramRecommendationResponse.BALANCED_3)
        self.assertEqual(
            client_type_7.get('priority_2'),
            ProgramRecommendationResponse.BALANCED_1)

        self.assertEqual(
            client_type_8.get('priority_1'),
            ProgramRecommendationResponse.BALANCED_6)
        self.assertEqual(
            client_type_8.get('priority_2'),
            ProgramRecommendationResponse.BALANCED_3)

        self.assertEqual(
            client_type_9.get('priority_1'),
            ProgramRecommendationResponse.TRIAL)
        self.assertEqual(client_type_9.get('priority_2'), None)

    def test_get_calculate_specific_nutrition_needs(self):
        percentage = 50
        daily_energy_needs = 500
        denominator = 10

        self.assertEqual(
            get_calculate_specific_nutrition_needs(
                percentage,
                daily_energy_needs,
                denominator),
            25)

    def test_protein_needs_condition_1(self):
        special_condition_choice = 3
        body_mass_index = 20
        daily_energy_needs = 500

        self.assertEqual(
            get_protein_needs(
                special_condition_choice,
                body_mass_index,
                daily_energy_needs),
            get_calculate_specific_nutrition_needs(
                13,
                daily_energy_needs,
                4))

    def test_protein_needs_condition_2(self):
        special_condition_choice = 1
        body_mass_index = 35
        daily_energy_needs = 450

        self.assertEqual(
            get_protein_needs(
                special_condition_choice,
                body_mass_index,
                daily_energy_needs),
            get_calculate_specific_nutrition_needs(
                18,
                daily_energy_needs,
                4))

    def test_fat_needs_condition_1(self):
        special_condition_choice = 3
        body_mass_index = 20
        daily_energy_needs = 500

        self.assertEqual(
            get_fat_needs(
                special_condition_choice,
                body_mass_index,
                daily_energy_needs),
            get_calculate_specific_nutrition_needs(
                30,
                daily_energy_needs,
                9))

    def test_fat_needs_condition_2(self):
        special_condition_choice = 1
        body_mass_index = 35
        daily_energy_needs = 450

        self.assertEqual(
            get_fat_needs(
                special_condition_choice,
                body_mass_index,
                daily_energy_needs),
            get_calculate_specific_nutrition_needs(
                27,
                daily_energy_needs,
                9))

    def test_carbohydrate_needs_condition_1(self):
        special_condition_choice = 3
        body_mass_index = 20
        daily_energy_needs = 500

        self.assertEqual(
            get_carbohydrate_needs(
                special_condition_choice,
                body_mass_index,
                daily_energy_needs),
            get_calculate_specific_nutrition_needs(
                57,
                daily_energy_needs,
                4))

    def test_carbohydrate_needs_condition_2(self):
        special_condition_choice = 1
        body_mass_index = 35
        daily_energy_needs = 450

        self.assertEqual(
            get_carbohydrate_needs(
                special_condition_choice,
                body_mass_index,
                daily_energy_needs),
            get_calculate_specific_nutrition_needs(
                55,
                daily_energy_needs,
                4))

    def test_get_fiber_needs(self):

        self.assertEqual(get_fiber_needs(), 25)

    def test_get_food_consumed_score(self):
        choice = 3

        self.assertEqual(get_food_consumed_score(choice), 2)

    def test_get_total_vegetable_and_fruit_score_with_valid_choice(self):
        vegetable_choice = 2
        fruit_choice = 3

        self.assertEqual(
            get_total_vegetable_and_fruit_score(
                vegetable_choice, fruit_choice), 3)

    def test_get_total_vegetable_and_fruit_score_with_invalid_vegetable_choice(
            self):
        with self.assertRaises(ValueError):
            vegetable_choice = 6
            fruit_choice = 2

            get_total_vegetable_and_fruit_score(vegetable_choice, fruit_choice)

    def test_get_total_vegetable_and_fruit_score_with_invalid_fruit_choice(
            self):
        with self.assertRaises(ValueError):
            vegetable_choice = 3
            fruit_choice = 0

            get_total_vegetable_and_fruit_score(vegetable_choice, fruit_choice)

    def test_get_vegetable_and_fruit_sufficiency_response_with_valid_total_score_condition_1(
            self):
        total_vegetable_and_fruit_score = 4

        self.assertEqual(
            get_vegetable_and_fruit_sufficiency_response(total_vegetable_and_fruit_score),
            VegetableAndFruitSufficiencyResponse.LACKING)

    def test_get_total_vegetable_and_fruit_sufficiency_response_with_valid_total_score_condition_2(
            self):
        total_vegetable_and_fruit_score = 7

        self.assertEqual(get_vegetable_and_fruit_sufficiency_response(
            total_vegetable_and_fruit_score), VegetableAndFruitSufficiencyResponse.ENOUGH)

    def test_get_total_vegetable_and_fruit_sufficiency_response_with_invalid_total_score(
            self):
        with self.assertRaises(ValueError):
            total_vegetable_and_fruit_score = 10

            get_vegetable_and_fruit_sufficiency_response(
                total_vegetable_and_fruit_score)

    def test_get_vegetable_and_fruit_diet_recommendation_condition_1(
            self):
        vegetable_and_fruit_suffiency_response = VegetableAndFruitSufficiencyResponse.LACKING

        self.assertEqual(
            get_vegetable_and_fruit_diet_recommendation(vegetable_and_fruit_suffiency_response),
            VegetableAndFruitDietRecommendation.LACKING)

    def test_get_vegetable_and_fruit_diet_recommendation_condition_2(
            self):
        vegetable_and_fruit_suffiency_response = VegetableAndFruitSufficiencyResponse.ENOUGH

        self.assertEqual(get_vegetable_and_fruit_diet_recommendation(
            vegetable_and_fruit_suffiency_response), VegetableAndFruitDietRecommendation.ENOUGH)

    def test_get_total_sugar_salt_fat_score_with_valid_choice(self):
        fried_food_choice = 1
        sweet_snacks_choice = 3
        sweet_drinks_choice = 5
        packaged_food_choice = 5

        self.assertEqual(
            get_total_sugar_salt_fat_score(
                fried_food_choice,
                sweet_snacks_choice,
                sweet_drinks_choice,
                packaged_food_choice),
            10)

    def test_get_total_sugar_salt_fat_score_with_invalid_fried_food_choice(
            self):
        with self.assertRaises(ValueError):
            fried_food_choice = 0
            sweet_snacks_choice = 3
            sweet_drinks_choice = 1
            packaged_food_choice = 2

            get_total_sugar_salt_fat_score(
                fried_food_choice,
                sweet_snacks_choice,
                sweet_drinks_choice,
                packaged_food_choice)

    def test_get_total_sugar_salt_fat_score_with_invalid_sweet_snacks_choice(
            self):
        with self.assertRaises(ValueError):
            fried_food_choice = 1
            sweet_snacks_choice = 6
            sweet_drinks_choice = 1
            packaged_food_choice = 2

            get_total_sugar_salt_fat_score(
                fried_food_choice,
                sweet_snacks_choice,
                sweet_drinks_choice,
                packaged_food_choice)

    def test_get_total_sugar_salt_fat_score_with_invalid_sweet_drinks_choice(
            self):
        with self.assertRaises(ValueError):
            fried_food_choice = 1
            sweet_snacks_choice = 2
            sweet_drinks_choice = 0
            packaged_food_choice = 2

            get_total_sugar_salt_fat_score(
                fried_food_choice,
                sweet_snacks_choice,
                sweet_drinks_choice,
                packaged_food_choice)

    def test_get_total_sugar_salt_fat_score_with_invalid_packaged_food_choice(
            self):
        with self.assertRaises(ValueError):
            fried_food_choice = 1
            sweet_snacks_choice = 2
            sweet_drinks_choice = 5
            packaged_food_choice = 6

            get_total_sugar_salt_fat_score(
                fried_food_choice,
                sweet_snacks_choice,
                sweet_drinks_choice,
                packaged_food_choice)

    def test_get_sugar_salt_fat_problem_response_with_invalid_total_score(
            self):
        with self.assertRaises(ValueError):
            total_sugar_salt_fat_score = 17

            get_sugar_salt_fat_problem_response(
                total_sugar_salt_fat_score)

    def test_get_sugar_salt_fat_problem_response_with_valid_total_score_condition_1(
            self):
        total_sugar_salt_fat_score = 6

        self.assertEqual(
            get_sugar_salt_fat_problem_response(total_sugar_salt_fat_score),
            SugarSaltFatProblemResponse.CONTROLLED)

    def test_get_sugar_salt_fat_problem_response_with_valid_total_score_condition_2(
            self):
        total_sugar_salt_fat_score = 10

        self.assertEqual(
            get_sugar_salt_fat_problem_response(total_sugar_salt_fat_score),
            SugarSaltFatProblemResponse.EXCESSIVE)

    def test_get_sugar_salt_fat_diet_recommendation_condition_1(
            self):
        sugar_salt_fat_problem_response = SugarSaltFatProblemResponse.CONTROLLED

        self.assertEqual(
            get_sugar_salt_fat_diet_recommendation(sugar_salt_fat_problem_response),
            SugarSaltFatDietRecommendation.CONTROLLED)

    def test_get_sugar_salt_fat_diet_recommendation_condition_2(
            self):
        sugar_salt_fat_problem_response = SugarSaltFatProblemResponse.EXCESSIVE

        self.assertEqual(
            get_sugar_salt_fat_diet_recommendation(sugar_salt_fat_problem_response),
            SugarSaltFatDietRecommendation.EXCESSIVE)

    def test_get_large_meal_diet_recommendation_with_invalid_choice(
            self):
        with self.assertRaises(ValueError):
            large_meal_choice = 5

            get_large_meal_diet_recommendation(large_meal_choice)

    def test_get_large_meal_diet_recommendation_with_valid_choice_condition_1(
            self):
        large_meal_choice = 1

        self.assertEqual(
            get_large_meal_diet_recommendation(large_meal_choice),
            LargeMealDietRecommendation.ONCE_A_DAY)

    def test_get_large_meal_diet_recommendation_with_valid_choice_condition_2(
            self):
        large_meal_choice = 2

        self.assertEqual(
            get_large_meal_diet_recommendation(large_meal_choice),
            LargeMealDietRecommendation.TWICE_A_DAY)

    def test_get_large_meal_diet_recommendation_with_valid_choice_condition_3(
            self):
        large_meal_choice = 3

        self.assertEqual(
            get_large_meal_diet_recommendation(large_meal_choice),
            LargeMealDietRecommendation.THRICE_A_DAY)

    def test_get_large_meal_diet_recommendation_with_valid_choice_condition_4(
            self):
        large_meal_choice = 4

        self.assertEqual(
            get_large_meal_diet_recommendation(large_meal_choice),
            LargeMealDietRecommendation.MORE_THAN_THRICE_A_DAY)

    def test_get_snacks_diet_recommendation_with_invalid_choice(
            self):
        with self.assertRaises(ValueError):
            snacks_choice = 0

            get_snacks_diet_recommendation(snacks_choice)

    def test_get_snacks_diet_recommendation_with_valid_choice_condition_1(
            self):
        snacks_choice = 1

        self.assertEqual(
            get_snacks_diet_recommendation(snacks_choice),
            SnacksDietRecommendation.NO_SNACK)

    def test_get_snacks_diet_recommendation_with_valid_choice_condition_2(
            self):
        snacks_choice = 2

        self.assertEqual(
            get_snacks_diet_recommendation(snacks_choice),
            SnacksDietRecommendation.ONCE_A_DAY)

    def test_get_snacks_diet_recommendation_with_valid_choice_condition_3(
            self):
        snacks_choice = 3

        self.assertEqual(
            get_snacks_diet_recommendation(snacks_choice),
            SnacksDietRecommendation.TWICE_A_DAY)

    def test_get_snacks_diet_recommendation_with_valid_choice_condition_4(
            self):
        snacks_choice = 4

        self.assertEqual(
            get_snacks_diet_recommendation(snacks_choice),
            SnacksDietRecommendation.THRICE_A_DAY)

    def test_get_snacks_diet_recommendation_with_valid_choice_condition_5(
            self):
        snacks_choice = 5

        self.assertEqual(
            get_snacks_diet_recommendation(snacks_choice),
            SnacksDietRecommendation.MORE_THAN_THRICE_A_DAY)


class QuizResultTests(APITestCase):
    def setUp(self):
        self.diet_profile = DietProfile.objects.create(
            name="test2",
            email="test2@test2.com",
            age=21,
            weight=61,
            height=173,
            gender=2,
            special_condition=1,
            body_activity=1,
            vegetables_in_one_day=2,
            fruits_in_one_day=1,
            fried_food_in_one_day=1,
            sweet_snacks_in_one_day=2,
            sweet_drinks_in_one_day=1,
            packaged_food_in_one_day=1,
            large_meal_in_one_day=1,
            snacks_in_one_day=2,
            breakfast_type=1,
            current_condition=1,
            problem_to_solve=1,
            health_problem=[2, 3])

        self.quiz_result = QuizResult.objects.create(
            diet_profile = self.diet_profile,
            age = self.diet_profile.age,
            weight = self.diet_profile.weight,
            height = self.diet_profile.height,
            gender = self.diet_profile.gender,
            body_mass_index = 22.99,
            nutrition_status = BodyMassConstants.NORMAL,
            ideal_weight_range = {'min': 100, 'max': 110},
            daily_energy_needs = 100.99,
            daily_nutrition_needs = {
                'protein_needs': 100.99, 'fat_needs': 100.99,
                'carbohydrate_needs': 100.99, 'fiber_needs': 25,
            },
            vegetable_and_fruit_sufficiency = VegetableAndFruitSufficiencyResponse.ENOUGH,
            vegetable_and_fruit_diet_recommendation = VegetableAndFruitDietRecommendation.ENOUGH,
            sugar_salt_fat_problem = 100.99,
            sugar_salt_fat_diet_recommendation = SugarSaltFatDietRecommendation.EXCESSIVE,
            large_meal_diet_recommendation = LargeMealDietRecommendation.MORE_THAN_THRICE_A_DAY,
            snacks_diet_recommendation = SnacksDietRecommendation.MORE_THAN_THRICE_A_DAY,
            breakfast_recommendation = BreakfastReponse.HI_BREAKFAST,
            energy_needed_per_dine = {
                'breakfast': 100.99, 'morning_snack': 100.99,
                'lunch': 100.99, 'afternoon_snack': 100.99,
                'dinner': 100.99,
            },
            physical_activity_recommendation = PhysicalActivityResponse.LEVEL1_ACTIVITY,
            program_recommendation = {
                'priority_1': ProgramRecommendationResponse.BALANCED_1,
                'priority_2': ProgramRecommendationResponse.TRIAL,
            }
        )

    def test_quiz_result_saved(self):
        self.assertEqual(QuizResult.objects.count(), 1)

    def test_quiz_result_string(self):
        self.assertEqual(str(self.quiz_result), str(self.diet_profile))
