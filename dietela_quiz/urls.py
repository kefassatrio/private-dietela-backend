from rest_framework import routers
from .views import DietProfileViewSet

router = routers.SimpleRouter()
router.register('dietela-quiz/diet-profile', DietProfileViewSet)

urlpatterns = []

urlpatterns += router.urls
