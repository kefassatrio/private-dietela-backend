from .formulas import (
    get_body_mass_index,
    get_nutrition_status,
    get_ideal_weight_range,
    get_basic_energy_needs,
    get_physical_activity_percentage,
    get_daily_energy_needs,
    get_protein_needs,
    get_fat_needs,
    get_carbohydrate_needs,
    get_fiber_needs,
    get_total_vegetable_and_fruit_score,
    get_vegetable_and_fruit_sufficiency_response,
    get_vegetable_and_fruit_diet_recommendation,
    get_total_sugar_salt_fat_score,
    get_sugar_salt_fat_problem_response,
    get_sugar_salt_fat_diet_recommendation,
    get_large_meal_diet_recommendation,
    get_snacks_diet_recommendation,
    get_breakfast_recommendation,
    get_energy_needed_per_dine,
    get_physical_activity_recommendation,
    get_program_recommendation,
)
from .models import (
    QuizResult
)

def create_or_update_quiz_result(profile):
    age = profile.age
    weight = profile.weight
    height = profile.height
    gender = profile.gender
    body_mass_index = get_body_mass_index(profile.weight,profile.height)
    nutrition_status = get_nutrition_status(body_mass_index)
    ideal_weight_range = get_ideal_weight_range(profile.height)
    basic_energy_needs = get_basic_energy_needs(profile.gender,
        profile.weight,profile.height,profile.age)
    physical_activity_percentage = get_physical_activity_percentage(
        profile.body_activity)
    daily_energy_needs = get_daily_energy_needs(profile.special_condition,
        nutrition_status,basic_energy_needs,physical_activity_percentage)
    protein = get_protein_needs(profile.special_condition,body_mass_index,
        daily_energy_needs)
    fat = get_fat_needs(profile.special_condition,body_mass_index,
        daily_energy_needs)
    carbohydrate = get_carbohydrate_needs(profile.special_condition,
        body_mass_index,daily_energy_needs)
    fiber = get_fiber_needs()
    daily_nutrition_needs = {'protein_needs': protein, 'fat_needs': fat,
        'carbohydrate_needs': carbohydrate, 'fiber_needs': fiber,}
    total_vegetable_and_fruit_score = get_total_vegetable_and_fruit_score(
        profile.vegetables_in_one_day,profile.fruits_in_one_day)
    vegetable_and_fruit_sufficiency = get_vegetable_and_fruit_sufficiency_response(
        total_vegetable_and_fruit_score)
    vegetable_and_fruit_diet_recommendation = \
        get_vegetable_and_fruit_diet_recommendation(vegetable_and_fruit_sufficiency)

    total_sugar_salt_fat_score = get_total_sugar_salt_fat_score(
        profile.fried_food_in_one_day,profile.sweet_snacks_in_one_day,
        profile.sweet_drinks_in_one_day,profile.packaged_food_in_one_day)

    sugar_salt_fat_problem = get_sugar_salt_fat_problem_response(
        total_sugar_salt_fat_score)
    sugar_salt_fat_diet_recommendation = \
        get_sugar_salt_fat_diet_recommendation(sugar_salt_fat_problem)
    large_meal_diet_recommendation = \
        get_large_meal_diet_recommendation(profile.large_meal_in_one_day)
    snacks_diet_recommendation = \
        get_snacks_diet_recommendation(profile.snacks_in_one_day)
    breakfast_recommendation = get_breakfast_recommendation(profile.breakfast_type)
    energy_needed_per_dine = get_energy_needed_per_dine(profile.breakfast_type,daily_energy_needs)
    physical_activity_recommendation = get_physical_activity_recommendation(profile.body_activity)
    program_recommendation = get_program_recommendation(profile.current_condition,
        profile.problem_to_solve,profile.health_problem)

    quiz_result = QuizResult.objects.update_or_create(
        diet_profile=profile,age=age,weight=weight,height=height,gender=gender,
        body_mass_index=body_mass_index,nutrition_status=nutrition_status,
        ideal_weight_range=ideal_weight_range,daily_energy_needs=daily_energy_needs,
        daily_nutrition_needs=daily_nutrition_needs,
        vegetable_and_fruit_sufficiency=vegetable_and_fruit_sufficiency,
        vegetable_and_fruit_diet_recommendation=vegetable_and_fruit_diet_recommendation,
        sugar_salt_fat_problem=sugar_salt_fat_problem,
        sugar_salt_fat_diet_recommendation=sugar_salt_fat_diet_recommendation,
        large_meal_diet_recommendation=large_meal_diet_recommendation,
        snacks_diet_recommendation=snacks_diet_recommendation,
        breakfast_recommendation=breakfast_recommendation,
        energy_needed_per_dine=energy_needed_per_dine,
        physical_activity_recommendation=physical_activity_recommendation,
        program_recommendation=program_recommendation
    )

    return quiz_result[0]
