from rest_framework import viewsets, status
from rest_framework.response import Response
from .serializers import DietProfileSerializer, QuizResultSerializer
from .models import DietProfile, QuizResult
from .utilities import create_or_update_quiz_result


class DietProfileViewSet(viewsets.ModelViewSet):
    serializer_class = DietProfileSerializer
    queryset = DietProfile.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        quiz_result = QuizResult.objects.get(diet_profile=instance)
        response = serializer.data
        response['quiz_result'] = QuizResultSerializer(quiz_result).data

        return Response(response, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        instance = serializer.save()
        create_or_update_quiz_result(instance)
        return instance
