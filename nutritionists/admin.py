from django.contrib import admin
from .models import Nutritionist

admin.site.register(Nutritionist)
