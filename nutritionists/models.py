from django.db import models


class Nutritionist(models.Model):
    full_name_and_degree = models.CharField(max_length=255)
    registration_certificate_no = models.CharField(max_length=255)
    university = models.CharField(max_length=255)
    mastered_nutritional_problems = models.TextField()
    handled_age_group = models.TextField()
    another_practice_place = models.CharField(max_length=255)
    languages = models.CharField(max_length=255)
    profile_picture = models.ImageField(upload_to='nutritionist',null=True)

    def __str__(self):
        return f"{self.full_name_and_degree}"
