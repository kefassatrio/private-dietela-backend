from rest_framework.test import APITestCase
from rest_framework import status
from .models import Nutritionist
from .serializers import NutritionistSerializer


class NutritionistTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.BASE_URL = "/nutritionists/"

        cls.nutritionist_1 = Nutritionist.objects.create(
            full_name_and_degree="Test, S.Gz",
            registration_certificate_no="1234567890",
            university="Universitas Indonesia",
            mastered_nutritional_problems="Manajemen berat badan, hipertensi",
            handled_age_group="12-17 tahun (Remaja)",
            another_practice_place="RSIU",
            languages="Bahasa Indonesia, Bahasa Inggris",
        )
        cls.nutritionist_2 = Nutritionist.objects.create(
            full_name_and_degree="Test 2, S.Gz",
            registration_certificate_no="0987654321",
            university="Universitas Indonesia",
            mastered_nutritional_problems="Diet diabetes melitus",
            handled_age_group="60 tahun keatas (Lansia)",
            another_practice_place="RSMC",
            languages="Bahasa Indonesia",
        )

    def test_get_all_nutritionists(self):
        response = self.client.get(self.BASE_URL)

        nutritionists = Nutritionist.objects.all()
        serializer = NutritionistSerializer(nutritionists, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_nutritionist(self):
        url = f"{self.BASE_URL}{self.nutritionist_1.pk}/"
        response = self.client.get(url)

        nutritionist = Nutritionist.objects.get(pk=self.nutritionist_1.pk)
        serializer = NutritionistSerializer(nutritionist)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_nutritionist(self):
        url = f"{self.BASE_URL}1303/"
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_string_nutritionist(self):
        self.assertEqual(str(self.nutritionist_1), 'Test, S.Gz')
