from rest_framework import routers
from .views import NutritionistViewSet

router = routers.SimpleRouter()
router.register('nutritionists', NutritionistViewSet)

urlpatterns = []

urlpatterns += router.urls
