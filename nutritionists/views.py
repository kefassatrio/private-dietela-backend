from rest_framework import viewsets
from .serializers import NutritionistSerializer
from .models import Nutritionist


class NutritionistViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = NutritionistSerializer
    queryset = Nutritionist.objects.all()
