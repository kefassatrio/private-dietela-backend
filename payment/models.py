from django.db import models
from dietela_program.models import DietelaProgram
from nutritionists.models import Nutritionist
from authentication.models import CustomUser


class Cart(models.Model):
    program = models.ForeignKey(
        DietelaProgram,
        on_delete=models.CASCADE
    )
    nutritionist = models.ForeignKey(
        Nutritionist,
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name="cart",
        null=True
    )

    def __str__(self):
        return f"{self.user} - {self.program.unique_code} - {self.nutritionist}"
