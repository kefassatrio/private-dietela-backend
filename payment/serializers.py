from rest_framework import serializers
from dietela_program.serializers import DietelaProgramSerializer
from nutritionists.serializers import NutritionistSerializer
from authentication.serializers import CustomUserDetailsSerializer
from .models import Cart



class CartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cart
        fields = "__all__"

    def to_representation(self, instance):
        result = super().to_representation(instance)
        result['program'] = DietelaProgramSerializer(instance.program).data
        result['nutritionist'] = NutritionistSerializer(instance.nutritionist).data
        result['user'] = CustomUserDetailsSerializer(instance.user).data
        return result
