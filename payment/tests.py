import json
from unittest.mock import patch
from rest_framework.test import APITestCase
from django.test import TestCase
from rest_framework import status
from rest_framework.response import Response
from .models import Cart
from .serializers import CartSerializer
from .views import PaymentViewSet
from authentication.models import CustomUser
from dietela_quiz.models import *
from dietela_program.models import DietelaProgram
from nutritionists.models import Nutritionist
from authentication.models import CustomUser


class CartTests(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.BASE_URL = "/payment/cart/"

        cls.dietela_program_1 = DietelaProgram.objects.create(
            unique_code="PRG1",
            name="Program 1",
            price=350000.00,
        )

        cls.dietela_program_2 = DietelaProgram.objects.create(
            unique_code="PRG2",
            name="Program 2",
            price=300000.00,
        )
        
        cls.nutritionist_1 = Nutritionist.objects.create(
            id=1,
            full_name_and_degree="Test, S.Gz",
            registration_certificate_no="1234567890",
            university="Universitas Indonesia",
            mastered_nutritional_problems="Manajemen berat badan, hipertensi",
            handled_age_group="12-17 tahun (Remaja)",
            another_practice_place="RSCM",
            languages="Bahasa Indonesia, Bahasa Inggris",
        )

        cls.cart_1 = Cart.objects.create(
            program = cls.dietela_program_1,
            nutritionist = cls.nutritionist_1,
        )

        cls.cart_2 = Cart.objects.create(
            program = cls.dietela_program_2,
            nutritionist = cls.nutritionist_1,
        )

    def test_get_all_carts(self):
        response = self.client.get(self.BASE_URL)

        cart = Cart.objects.all()
        serializer = CartSerializer(cart, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_cart(self):
        url = f"{self.BASE_URL}{self.cart_1.pk}/"
        response = self.client.get(url)

        cart = Cart.objects.get(
            pk=self.cart_1.pk)
        serializer = CartSerializer(cart)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_cart(self):
        url = f"{self.BASE_URL}1303/"
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_cart_string(self):
        self.assertEqual(str(self.cart_1), 'None - PRG1 - Test, S.Gz')
    
    def test_post_cart_api(self):
        url = f"{self.BASE_URL}"
        data = {
            'program': 'PRG1',
            'nutritionist': 1,
        }
        response = self.client.post(
            url, data, format='json')
        self.assertEqual(json.loads(response.content).get('program').get('unique_code'), 'PRG1')
        self.assertEqual(Cart.objects.count(), 3)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

@patch('payment.views.json.loads')
class TestPaymentIntegration(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.PAYMENT_URL = "/payment/midtrans/"
        
        cls.nutritionist = Nutritionist.objects.create(
            full_name_and_degree="Test, S.Gz",
            registration_certificate_no="1234567890",
            university="Universitas Indonesia",
            mastered_nutritional_problems="Manajemen berat badan, hipertensi",
            handled_age_group="12-17 tahun (Remaja)",
            another_practice_place="RSCM",
            languages="Bahasa Indonesia, Bahasa Inggris",
        )

        cls.dietela_program = DietelaProgram.objects.create(
            unique_code="PRG1",
            name="Program 1",
            price=350000.00,
        )

        cls.custom_user = CustomUser.objects.create_user(name='tes', email='email@email.com', password='abc')

        cls.cart = Cart.objects.create(
            program = cls.dietela_program,
            nutritionist = cls.nutritionist,
            user = cls.custom_user
        )

    def test_post_payment_succeed(self, mock_json_loads):
        mock_json_return_value = {'token': 'abc', 'redirect_url': 'https://redirect.url'}
        mock_json_loads.return_value = mock_json_return_value
        response = self.client.post(self.PAYMENT_URL, {'cart_id': self.cart.id}, format='json')
        json_response = json.loads(response.content)
        self.assertIn('token', json_response)
        self.assertIn('redirect_url', json_response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post_payment_failed_because_cart_has_been_paid(self, mock_json_loads):
        mock_json_return_value = {'error_messages':
            ["transaction_details.order_id sudah digunakan"]
        }
        mock_json_loads.return_value = mock_json_return_value
        response = self.client.post(self.PAYMENT_URL, {'cart_id': self.cart.id}, format='json')
        json_response = json.loads(response.content)
        self.assertNotIn('token', json_response)
        self.assertNotIn('redirect_url', json_response)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
