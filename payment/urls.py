from rest_framework import routers
from .views import CartViewSet, PaymentViewSet

router = routers.SimpleRouter()
router.register('payment/cart', CartViewSet)
router.register('payment/midtrans', PaymentViewSet, \
    basename="Payment")

urlpatterns = []

urlpatterns += router.urls
