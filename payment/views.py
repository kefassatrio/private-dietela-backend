import json
import os
from rest_framework import viewsets, status
from rest_framework.response import Response
import requests
from dietela_program.serializers import DietelaProgramSerializer
from dietela_program.models import DietelaProgram
from .serializers import CartSerializer
from .models import Cart



class CartViewSet(viewsets.ModelViewSet):
    serializer_class = CartSerializer
    queryset = Cart.objects.all()

    def create(self, request, *args, **kwargs):
        program_unique_id = request.data['program']
        program = DietelaProgram.objects.get(unique_code=program_unique_id)
        request.data['program']=program.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        program = DietelaProgram.objects.get(unique_code=program_unique_id)
        response = serializer.data
        response['program'] = DietelaProgramSerializer(program).data

        return Response(response, status=status.HTTP_201_CREATED, headers=headers)


class PaymentViewSet(viewsets.ViewSet):

    MIDTRANS_URL = "https://app.sandbox.midtrans.com/snap/v1/transactions"
    POST_PAYMENT_HEADERS = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Basic {os.environ.get('AUTH_STRING')}"
    }

    @staticmethod
    def create_payload(cart):
        payload = {
            "transaction_details": {
                "order_id": cart.id,
                "gross_amount": cart.program.price,
            },
            "item_details": [{
                "id": cart.program.unique_code,
                "price": cart.program.price,
                "quantity": 1,
                "name": cart.program.name,
            }],
            "customer_details": {
                "name": cart.user.name,
                "email": cart.user.email,
            }
        }

        return json.dumps(payload)


    def create(self, request):
        cart_id = request.data['cart_id']
        cart = Cart.objects.get(pk=cart_id)
        payload = self.create_payload(cart)
        response = requests.post(self.MIDTRANS_URL, payload, headers=self.POST_PAYMENT_HEADERS)
        json_response = json.loads(response.content)

        if not('token' in json_response and 'redirect_url' in json_response):
            return Response(json_response, status=status.HTTP_400_BAD_REQUEST)

        return Response(json_response, status=status.HTTP_200_OK)
